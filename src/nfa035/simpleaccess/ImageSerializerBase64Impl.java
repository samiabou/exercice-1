package nfa035.simpleaccess;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

public class ImageSerializerBase64Impl implements ImageSerializer {
    /**
     * Sérialise une image Base64
     *
     * @param image
     * @return
     * @throws IOException;
     */
    @Override
    public String serialize(File image)throws IOException {
        // TODO Auto-generated method stub
        byte[] fichier = Files.readAllBytes(image.toPath());
        String encoded = Base64.getEncoder().encodeToString(fichier);

        return encoded;
    }

    /**
     * Désérialise une image Base64
     * @param encodedImage
     * @return
     */
    @Override
    public byte[] deserialize(String encodedImage) {
        // TODO Auto-generated method stub
        byte[] decoded = Base64.getDecoder().decode(encodedImage);
        return decoded;
    }


}


