package nfa035.simpleaccess;

import java.io.File;
import java.io.IOException;



/**
 * Interface pour sérialiser et désérialiser l'image
 */

public interface ImageSerializer {

    /**
     * Sérialisation
     *
     * @param image
     * @return
     * @throws IOException
     */
    String serialize(File image) throws IOException;

    /**
     * Désérialisation
     *
     * @param encodedImage
     * @return
     */

    byte[] deserialize(String encodedImage);
}

