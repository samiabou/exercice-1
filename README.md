### Sérialisation d'une image en Base64

Vous trouverez dans ce fichier un programme pour Lire et sérialiser une image à un format texte : en base 64

## Contenu

 - [ ] Class **SimpleTestAccess**
 - [ ] Interface Java **ImageSerializer**
 - [ ] Class **ImageSerializerBase64Impl**
 - [ ] un fichier texte le résultat de la sérialisation.
 - [ ] petite_image.png "Echantillon principal"
